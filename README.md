# Advent of Code: 2020

My solutions to [advent of code 2020](https://adventofcode.com/2020).

| Language | Days Solved                   |
|----------|-------------------------------|
| Python   | 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 |
| Rust     | 1                             |


## Requirements

- Python (has only been tested against 3.8.3)
- rustc (has only been tested against 1.48.0)

## File Strucutre

- Solutions are stored in `solutions/$programming-language/`.
- Input files are stored in `inputs/`

## Running

Each solution takes the input file from stdin.

    $ ./solutions/python/01-solution.py < ./inputs/01-input

Solutions in rust will need compiling first.

    $ rustc ./solutions/rust/01-solution.rs
    $ ./01-solution < ./inputs/01-input

