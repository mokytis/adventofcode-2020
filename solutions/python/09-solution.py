#!/usr/bin/env python

import fileinput

import utils


def parse_input():
    data = []
    for line in fileinput.input():
        if line := line.strip():
            data.append(int(line))
    return data


def part1(data, preamble=25):
    ...
    for i, n in enumerate(data[25:]):
        for num in data[i : i + 25]:
            if n - num != num and n - num in data[i : i + 25]:
                break
        else:
            return n


def part2(data):
    target = part1(data)

    for i, n in enumerate(data):
        j = 1
        sum_i_j = data[i]
        while sum_i_j < target and j < len(data):
            sum_i_j += data[i + j]
            if sum_i_j == target:
                return min(data[i : i + j]) + max(data[i : i + j])
            j += 1


def main():
    header = utils.generate_header(9, "Encoding Error")
    print(header)

    data = parse_input()

    part1_ans = part1(data)
    print(f"Part 1: {part1_ans}")

    part2_ans = part2(data)
    print(f"Part 2: {part2_ans}")


if __name__ == "__main__":
    main()
