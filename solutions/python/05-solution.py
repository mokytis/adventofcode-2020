#!/usr/bin/env python

import fileinput

import utils


def parse_input():
    data = []
    for line in fileinput.input():
        if line := line.strip():
            data.append(line)
    return data


def get_seat_id(string):
    rl = 0
    rh = 128
    cl = 0
    ch = 8
    row = -1
    col = -1
    for i, x in enumerate(string[:7]):
        if x == "B":
            rl += (rh - rl) // 2
            row = rl
        elif x == "F":
            rh -= (rh - rl) // 2
            row = rh
        if i == 6:
            if x == "F":
                row -= 1
    for i, x in enumerate(string[7:]):
        if x == "R":
            cl += (ch - cl) // 2
            col = cl
        elif x == "L":
            ch -= (ch - cl) // 2
            col = ch
        if i == 2:
            if x == "L":
                col -= 1

    seat_id = (row * 8) + col
    return seat_id


def part1(data):
    return max([get_seat_id(line) for line in data])


def part2(data):
    do_exist = set(get_seat_id(line) for line in data)

    a = 0 in do_exist
    b = 1 in do_exist
    c = 2 in do_exist

    for seat_id in range(3, 1025):
        exist = seat_id in do_exist
        a = b
        b = c
        c = exist
        if a == True and c == True and b == False:
            return seat_id - 1


def main():
    header = utils.generate_header(5, "Binary Boarding")
    print(header)

    data = parse_input()

    part1_ans = part1(data)
    print(f"Part 1: {part1_ans}")

    part2_ans = part2(data)
    print(f"Part 2: {part2_ans}")


if __name__ == "__main__":
    main()
