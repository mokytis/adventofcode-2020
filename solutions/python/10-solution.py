#!/usr/bin/env python

import fileinput

import utils


def parse_input():
    data = set()
    max_input = -1
    for line in fileinput.input():
        if line := line.strip():
            num = int(line)
            data |= {num}
            if num > max_input:
                max_input = num
    return data, max_input


def part1(data):
    diff_1j = 0
    diff_3j = 0
    jolt = 0
    while True:

        if jolt + 1 in data:
            diff_1j += 1
            jolt += 1
        elif jolt + 2 in data:
            jolt += 2
        elif jolt + 3 in data:
            jolt += 3
            diff_3j += 1
        else:
            break

    jolt += 3
    diff_3j += 1
    print(jolt, diff_1j, diff_3j)
    return diff_1j * diff_3j


def search(data, jolt, memo, t):
    if jolt == t:
        return 1
    if jolt in memo:
        return memo[jolt]

    ways = 0
    if jolt + 1 in data:
        ways += search(data, jolt + 1, memo, t)
    if jolt + 2 in data:
        ways += search(data, jolt + 2, memo, t)
    if jolt + 3 in data:
        ways += search(data, jolt + 3, memo, t)
    memo[jolt] = ways
    return ways


def part2(data, target):
    memo = {}

    jolt = 0
    ways_to_arrange = search(data, jolt, memo, target)
    return ways_to_arrange


def main():
    header = utils.generate_header(10, "Adapter Array")
    print(header)

    data, max_input = parse_input()

    part1_ans = part1(data)
    print(f"Part 1: {part1_ans}")

    part2_ans = part2(data, max_input)
    print(f"Part 2: {part2_ans}")


if __name__ == "__main__":
    main()
