#!/usr/bin/env python

import fileinput

import utils


def parse_input():
    data = []
    for line in fileinput.input():
        if line.strip():
            data.append(line)
    return data


def part1(data, dx=3, dy=1):
    x = y = 0
    trees = 0
    while y < len(data):
        if data[y][x] == "#":
            trees += 1
        x += dx
        y += dy
        x = x % (len(data[0]) - 1)
    return trees


def part2(data):
    slopes = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]
    ans = 1
    for s in slopes:
        ans *= part1(data, s[0], s[1])
    return ans


def main():
    header = utils.generate_header(3, "Toboggan Trajectory")
    print(header)

    data = parse_input()

    part1_ans = part1(data)
    print(f"Part 1: {part1_ans}")

    part2_ans = part2(data)
    print(f"Part 2: {part2_ans}")


if __name__ == "__main__":
    main()
