#!/usr/bin/env python

import fileinput

import utils


def parse_input():
    data = {}
    for line in fileinput.input():
        if line := line.strip():
            outer, contents = line[:-1].split(" contain ")
            data[outer[:-5]] = set(
                map(
                    lambda b: b.replace("bags", "bag").replace(" bag", ""),
                    contents.split(", "),
                )
            ) - {"no other"}
    return data


def search_tree(data, node, search):
    for child in data[node]:
        child = child[2:]
        if child == search:
            return True
        if child in data:
            has_search = search_tree(data, child, search)
            if has_search:
                return True
    return False


def bags_in(data, node, memo):
    if node in memo:
        return memo[node]

    bags = 1
    for child in data[node]:
        num_of_child = int(child[0])
        child = child[2:]

        child_c = bags_in(data, child, memo)
        bags += child_c * num_of_child

    memo[node] = bags
    return bags


def part1(data):
    total = 0
    for row in data:
        has_shiny_gold = search_tree(data, row, "shiny gold")
        if has_shiny_gold:
            total += 1

    return total


def part2(data):
    memo = {}
    return bags_in(data, "shiny gold", memo) - 1


def main():
    header = utils.generate_header(7, "Handy Haversacks")
    print(header)

    data = parse_input()

    part1_ans = part1(data)
    print(f"Part 1: {part1_ans}")

    part2_ans = part2(data)
    print(f"Part 2: {part2_ans}")


if __name__ == "__main__":
    main()
