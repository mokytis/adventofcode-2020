#!/usr/bin/env python

import fileinput
import copy

import utils


def parse_input():
    data = []
    for line in fileinput.input():
        if line := line.strip():
            data.append(line)
    return data


def execute(instruction, pc, acc):
    command, arg = instruction.split()
    if command == "acc":
        acc += int(arg)
        pc += 1
    elif command == "jmp":
        pc += int(arg)
    elif command == "nop":
        pc += 1
    return pc, acc


def part1(data):
    acc = 0
    pc = 0
    visited = set()

    while pc not in visited:
        visited |= {pc}
        pc, acc = execute(data[pc], pc, acc)
    return acc


def part2(data):
    for i in range(len(data)):
        cmd, arg = data[i].split()
        if cmd == "nop":
            cmd = "jmp"
        elif cmd == "jmp":
            cmd = "nop"
        else:
            continue
        instructions = copy.copy(data)
        instructions[i] = f"{cmd} {arg}"

        has_executed = set()
        pc = acc = 0

        while pc not in has_executed or pc > len(instructions):
            has_executed |= {pc}
            pc, acc = execute(instructions[pc], pc, acc)

            if pc == len(instructions):
                return acc


def main():
    header = utils.generate_header(8, "Handheld Halting")
    print(header)

    data = parse_input()

    part1_ans = part1(data)
    print(f"Part 1: {part1_ans}")

    part2_ans = part2(data)
    print(f"Part 2: {part2_ans}")


if __name__ == "__main__":
    main()
