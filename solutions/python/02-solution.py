#!/usr/bin/env python

import itertools
import fileinput

import utils


def solve():
    valid_passwords_part_1 = 0
    valid_passwords_part_2 = 0
    for line in fileinput.input():
        if line.strip():
            head, password = line.split(": ")
            count_range, letter = head.split(" ")
            i, j = map(int, count_range.split("-"))

            if i <= password.count(letter) <= j:
                valid_passwords_part_1 += 1

            if [password[i - 1], password[j - 1]].count(letter) == 1:
                valid_passwords_part_2 += 1

    return valid_passwords_part_1, valid_passwords_part_2


def main():
    header = utils.generate_header(2, "Password Philosophy")
    print(header)

    part1_ans, part2_ans = solve()
    print(f"Part 1: {part1_ans}")
    print(f"Part 2: {part2_ans}")


if __name__ == "__main__":
    main()
