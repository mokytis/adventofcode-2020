#!/usr/bin/env python

import itertools
import fileinput

import utils


def parse_input():
    data = []
    for line in fileinput.input():
        if line.strip().isnumeric():
            data.append(int(line))
    return data


def part1(nums, candidates, target_sum=2020):
    for a, b in itertools.combinations(nums, 2):
        sum_ab = a + b
        if sum_ab == target_sum:
            return a * b

        # for every combination of a + b we work out what value (c) would be
        # needed for a + b +c == 20

        # we then store it in a dict with the values (a, b)
        # this makes finding the soluton to part 2 faster as we can just check
        # to see what value (c) in nums exists in candidates

        candidates[2020 - sum_ab] = (a, b)


def part2(nums, candidates, target_sum=2020):
    for c in nums:
        if c in candidates:
            return c * candidates[c][0] * candidates[c][1]


def main():
    header = utils.generate_header(1, "Report Repair")
    print(header)

    data = parse_input()
    candidates = {}  # will be populated when p1 ans is computed to assist p2

    part1_ans = part1(data, candidates)
    print(f"Part 1: {part1_ans}")

    part2_ans = part2(data, candidates)
    print(f"Part 2: {part2_ans}")


if __name__ == "__main__":
    main()
