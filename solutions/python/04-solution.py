#!/usr/bin/env python

import fileinput
import string

import utils


def parse_input():
    passport_fields = set()
    passport = {}
    data = []
    for line in fileinput.input():
        if line := line.strip():
            for f in line.split(" "):
                k, v = f.split(":")
                passport_fields |= {k}
                passport.update({k: v})
        else:
            data.append((passport_fields, passport))
            passport_fields = set()
            passport = {}
    return data


def part1(data):
    valid_ids = 0
    required = {"byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"}
    for fields, _ in data:
        if required.issubset(fields):
            valid_ids += 1

    return valid_ids


def part2(data):
    valid_ids = 0
    eye_colors = {"amb", "blu", "brn", "gry", "grn", "hzl", "oth"}
    required = {"byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"}
    i = 0
    for fields, passport in data:
        i += 1
        if required.issubset(fields):
            byr = int(passport["byr"])
            iyr = int(passport["iyr"])
            eyr = int(passport["eyr"])
            hgt_unit = passport["hgt"][-2:]
            hgt_value = int(passport["hgt"][:-2]) if passport["hgt"][:-2] else -1
            hcl = passport["hcl"]
            ecl = passport["ecl"]
            pid = passport["pid"]

            if not (1920 <= byr <= 2002):
                continue
            if not (2010 <= iyr <= 2020):
                continue
            if not (2020 <= eyr <= 2030):
                continue
            if hgt_unit == "cm":
                if not (150 <= hgt_value <= 193):
                    continue
            elif hgt_unit == "in":
                if not (59 <= hgt_value <= 76):
                    print("BHT", hgt_value)
                    continue
            else:
                continue
            if hcl[0] == "#" and len(hcl) == 7:
                bad_hair = False
                for c in hcl[1:]:
                    if not (c in string.hexdigits):
                        bad_hair = True
                        continue
                if bad_hair:
                    continue
            else:
                continue
            if not (ecl in eye_colors):
                continue
            if not (len(pid) == 9 and pid.isnumeric()):
                continue
            valid_ids += 1
    return valid_ids


def main():
    header = utils.generate_header(4, "Passport Processing")
    print(header)

    data = parse_input()

    part1_ans = part1(data)
    print(f"Part 1: {part1_ans}")

    part2_ans = part2(data)
    print(f"Part 2: {part2_ans}")


if __name__ == "__main__":
    main()
