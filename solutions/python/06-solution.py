#!/usr/bin/env python

import fileinput

import utils


def parse_input():
    data = []
    group = []
    for line in fileinput.input():
        if line := line.strip():
            group.append(set(line))
        elif group:
            data.append(group)
            group = []
    return data


def part1(data):
    total = 0
    for group in data:
        each_yes = set()
        for person in group:
            each_yes |= person
        total += len(each_yes)
    return total


def part2(data):
    total = 0
    for group in data:
        all_yes = set(group[0])
        for person in group:
            all_yes &= person
        total += len(all_yes)
    return total


def main():
    header = utils.generate_header(6, "Custom Customs")
    print(header)

    data = parse_input()

    part1_ans = part1(data)
    print(f"Part 1: {part1_ans}")

    part2_ans = part2(data)
    print(f"Part 2: {part2_ans}")


if __name__ == "__main__":
    main()
