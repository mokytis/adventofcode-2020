use std::collections::HashMap;
use std::io::{self, BufRead};

fn main() {
    let stdin = io::stdin();
    let mut data = Vec::new();
    let mut candidates = HashMap::new();

    // read input
    for line in stdin.lock().lines() {
        let num: i32 = line.unwrap().parse().unwrap();
        data.push(num);
    }

    // part 1
    'outer: for (i, num1) in data.iter().enumerate() {
        for num2 in data[i..].iter() {
            let sum = num1 + num2;
            if sum == 2020 {
                println!("{}", num1 * num2);
                break 'outer;
            }
            // store possible values of (num3) where num1 + num2 + num3 == 2020
            // this saves us iteterating over everything again
            candidates.insert(2020 - sum, (num1, num2));
        }
    }

    // part 2
    for num3 in data.iter() {
        match candidates.get(num3) {
            None => continue,
            Some(&values) => {
                println!("{}", *values.0 * *values.1 * num1);
                break;
            }
        }
    }
}
